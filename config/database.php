<?php

class DB {
    private static $connection = null;
    private static $host = 'eu-cdbr-west-02.cleardb.net';
    private static $db   = 'heroku_c484a063dfae272';
    private static $user = 'bab8450791d3eb';
    private static $pass = '5faa6c18';
    private static $charset = 'utf8';
    private static $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ];

    public static function connect() {
        if (!DB::$connection)
        {
            $dsn = "mysql:host=" . DB::$host . ";dbname=" . DB::$db . ";charset=" . DB::$charset . ";";
            DB::$connection = new PDO($dsn, DB::$user, DB::$pass, DB::$opt);
        }
    }

    public static function getConnection() {
        return DB::$connection;
    }

    public static function closeConnection() {
        DB::$connection = null;
    }
}
