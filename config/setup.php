<?php

include 'database.php';

DB::connect();

function dropTables() {
    $connection = DB::getConnection();

    $res = $connection->prepare("DROP TABLE IF EXISTS `user`");
    if($res->execute()){
        echo "Table deleted user\n";
    }else{
        print_r($connection->errorInfo());
    }

    $res = $connection->prepare("DROP TABLE IF EXISTS image");
    if($res->execute()){
        echo "Table deleted image\n";
    }else{
        print_r($connection->errorInfo());
    }

    $res = $connection->prepare("DROP TABLE IF EXISTS comment");
    if($res->execute()){
        echo "Table deleted comment\n";
    }else{
        print_r($connection->errorInfo());
    }

    $res = $connection->prepare("DROP TABLE IF EXISTS `like`");
    if($res->execute()){
        echo "Table deleted like\n";
    }else{
        print_r($connection->errorInfo());
    }
}

function createUserTable() {
    $table = "user";
    $connection = DB::getConnection();
    $sql = "CREATE table $table(
              id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
              username VARCHAR( 50 ) UNIQUE NOT NULL,
              email VARCHAR( 250 ) UNIQUE NOT NULL,
              password VARCHAR( 200 ) NOT NULL,
              isActive INT(1) NOT NULL DEFAULT 0,
              isNotification INT(1) NOT NULL DEFAULT 1,
              activationLink VARCHAR( 200 ) DEFAULT NULL,
              resetLink VARCHAR( 200 ) DEFAULT NULL
              );";
    try {
        $prepared = $connection->prepare($sql);
        $prepared->bindParam(':table', $table);
        $prepared->execute();
        echo "Table " . $table . " created\n";
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
}

function createImageTable() {
    $table = "image";
    $connection = DB::getConnection();
    $sql = "CREATE table $table(
              id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR( 250 ) UNIQUE NOT NULL,
              user INT( 11 ) NOT NULL
              );";

    try {
        $prepared = $connection->prepare($sql);
        $prepared->execute();
        echo "Table " . $table . " created\n";
    } catch(PDOException $e) {
        
    }
}

function createCommentTable() {
    $table = "comment";
    $connection = DB::getConnection();
    $sql = "CREATE table $table (
              id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
              image INT( 11 ) NOT NULL,
              user INT( 11 ) NOT NULL,
              text VARCHAR( 250 ) NOT NULL
              );";
    try {
        $prepared = $connection->prepare($sql);
        $prepared->execute();
        echo "Table " . $table . " created\n";
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
}

function createLikeTable() {
    $table = "`like`";
    $connection = DB::getConnection();
    $sql = "CREATE table $table (
              id INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
              image INT( 11 ) NOT NULL,
              user INT( 11 ) NOT NULL
              );";

    try {
        $prepared = $connection->prepare($sql);
        $prepared->execute();
        echo "Table like created\n";
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
}

dropTables();
createUserTable();
createImageTable();
createCommentTable();
createLikeTable();
