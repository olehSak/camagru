#!/bin/bash

DOCROOT="$(pwd)/assets"
INIFILE="$(pwd)/server/server.ini"
ROUTER="$(pwd)/index.php"
HOST=localhost
PORT=8000

PHP=$(which php)
if [ $? != 0 ] ; then
echo "Unable to find PHP"
exit 1
fi

$PHP -S $HOST:$PORT -c $INIFILE $ROUTER
