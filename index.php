<?php

include_once 'Router.php';
include_once 'Request.php';

include_once 'ViewGenerator.php';
include_once './config/database.php';
include_once './models/user.model.php';
include_once './models/gallery.model.php';

ini_set('display_errors', 1);
session_start();

function isStaticFile() {
    $path = parse_url($_SERVER['REQUEST_URI'])['path'];

    $url = explode("/", $path);

    if ($url[1] == 'assets') {
        if (is_file(__DIR__ . $path))
            return true;
    }
    return false;
}

if (isStaticFile())
    return false;

DB::connect();
$router = new Router(new Request());

$router->get('/', function($request) {
    $params = $request->getParams();
    UserModel::activate($params['activateLink']);
    $resetLink = UserModel::resetLinkExist($params['resetLink']);
    ViewGenerator::generate('main.php', $resetLink);
});

$router->post('/login', function($request) {
    $body = $request->getBody();
    $errors = UserModel::login(
        $body->{'username'},
        $body->{'password'}
    );
    $_SESSION['userId'] = $errors['id'];
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode($errors));
});

$router->get('/logout', function($request) {
    unset($_SESSION['userId']);
});

$router->post('/signup', function($request) {
    $body = $request->getBody();
    $errors = UserModel::create(
        $body->{'email'},
        $body->{'username'},
        $body->{'password'},
        $body->{'confPassword'}
    );
    header('Content-Type: application/json;charset=utf-8');
    if ($errors)
        print(json_encode($errors));
    else
        http_response_code(200);
});

$router->post('/forgotPassword', function($request) {
    $params = $request->getParams();
    UserModel::sendResetLink($params["email"]);
});

$router->post('/resetPassword', function($request) {
    $params = $request->getParams();
    $body = $request->getBody();
    print(json_encode(UserModel::resetPassword($params['resetLink'], $body->{'password'}, $body->{'confPassword'})));
});

$router->get('/gallery', function($request) {
    $params = $request->getParams();
    $images = GalleryModel::getImages($params['page'], $params['count']);
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode($images));
});

$router->post('/edit', function($request) {
    $body = $request->getBody();
    $result = UserModel::editUserById(
        $_SESSION['userId'],
        $body->{'email'},
        $body->{'username'},
        $body->{'password'},
        $body->{'confPassword'},
        $body->{'isNotification'});
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode($result));
});

$router->get('/gallery/image', function($request) {
    $params = $request->getParams();
    $info = GalleryModel::getImageInfoById($params['id']);
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode($info));
});

$router->delete('/gallery/image', function($request) {
    $params = $request->getParams();
    $info = GalleryModel::removeImageById($params['id'], $_SESSION['userId']);
    if (!$info) {
        http_response_code(400);
        return;
    }
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode($info));
});

$router->post('/gallery/image/comment', function($request) {
    $params = $request->getParams();
    $body = $request->getBody();
    $userId = $_SESSION['userId'];

    if (!$userId) { http_response_code(401); return;}

    $isOk = GalleryModel::commentImageById($params['id'], $body->{'text'}, $userId);
    if ($isOk)
        http_response_code(200);
    else
        http_response_code(400);
});

$router->post('/gallery/image/like', function($request) {
    $params = $request->getParams();
    $userId = $_SESSION['userId'];

    // if (!$userId) { http_response_code(401); return;}
    $isLike = GalleryModel::setLikeImageById($params['id'], $userId);
    header('Content-Type: application/json;charset=utf-8');
    print(json_encode(array("like" => $isLike)));
});

$router->post('/gallery', function($request) {
    $body = $request->getBody();
    if ($_SESSION['userId'])
        GalleryModel::createImage($_SESSION['userId'], $body);
    else
        http_response_code(401);
});
