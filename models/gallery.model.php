<?php

class GalleryModel
{
    public static function saveImgInDatabase($userId, $imgName) {
        $connection = DB::getConnection();
        $sql = "INSERT INTO image (user, name)
                  VALUES (?, ?)";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$userId, $imgName]);
            return null;
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    public static function getImageCommentsById($id){
        $connection = DB::getConnection();
        $sql = "SELECT comment.id, comment.text, user.username
                FROM comment
                INNER JOIN user ON comment.user=user.id
                WHERE comment.image=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$id]);
            $res = $prepared->fetchAll();
            return $res;
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    public static function isUserLikeImage($imageId, $userId){
        $connection = DB::getConnection();
        $sql = "SELECT * FROM `like` where image=? AND user=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$imageId, $userId]);
            $res = $prepared->fetch(PDO::FETCH_ASSOC);
            return $res ? true : false;
        } catch (PDOException $e) {
            return false;
        }
    }

    public static function getImageLikesById($id){
        $connection = DB::getConnection();
        $sql = "SELECT COUNT(*) FROM `like` WHERE image=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$id]);
            $res = $prepared->fetch(PDO::FETCH_BOTH)[0];
            return $res;
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    public static function getImageInfoById($id) {
        $comments = GalleryModel::getImageCommentsById($id);
        $likes = GalleryModel::getImageLikesById($id);
        $myLike = GalleryModel::isUserLikeImage($id, 1);

        return array(
            'comments' => $comments,
            'likes' => $likes,
            'myLike' => $myLike
        );
    }

    public static function removeImageById($id, $userId){
        $connection = DB::getConnection();
        $sql = "SELECT *  FROM image WHERE id = ? AND user = ?";
        $sql2 = "DELETE FROM comment WHERE image = ?";
        $sql3 = "DELETE FROM `like` WHERE image = ?";
        $sql4 = "DELETE FROM image WHERE id = ?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$id, $userId]);
            $image = $prepared->fetch();
            if ($image) {
                $prepared = $connection->prepare($sql2);
                $prepared->execute([$id]);
                $prepared = $connection->prepare($sql3);
                $prepared->execute([$id]);
                $prepared = $connection->prepare($sql4);
                $prepared->execute([$id]);
                unlink('assets/' . $image['name']);
                return true;
            }
            else return false;
        } catch(PDOException $e) {
            return false;
        }
    }

    public static function notifyImageOwner($imgId, $message) {
        $connection = DB::getConnection();
        $sql = "SELECT image.id, user.isNotification, user.email
                FROM image
                INNER JOIN user ON image.user=user.id
                WHERE image.id=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$imgId]);

            $user = $prepared->fetch(PDO::FETCH_ASSOC);
            if ($user && $user['isNotification']) {
                mail($user['email'], "Camagru Notification", $message);
            }
        } catch(PDOException $e) {
            return null;
        }
    }

    public static function commentImageById($id, $text, $userId) {
        if ($text) {
            $connection = DB::getConnection();
            $sql = "INSERT INTO comment (user, image, text)
                  VALUES (?, ?, ?)";

            try {
                $prepared = $connection->prepare($sql);
                $prepared->execute([$userId, $id, $text]);
                GalleryModel::notifyImageOwner($id, "Hi, some one comment your Image go and check it");
                return true;
            } catch(PDOException $e) {
                return false;
            }
        }
    }

    public static function likeImageById($id, $userId) {
        $connection = DB::getConnection();
        $sql = "INSERT INTO `like` (user, image)
              VALUES (?, ?)";
        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$userId, $id]);
            GalleryModel::notifyImageOwner($id,'Hey you Some one like Your Image check it NOW');
        } catch(PDOException $e) {
        }
    }

    public static function dislikeImageById($id, $userId) {
        $connection = DB::getConnection();
        $sql = "DELETE FROM `like` WHERE user=? AND image=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$userId, $id]);
        } catch(PDOException $e) {
        }
    }

    public static function setLikeImageById($id, $userId) {
        if (!GalleryModel::isUserLikeImage($id, $userId)){
            GalleryModel::likeImageById($id, $userId);
            return true;
        }
        GalleryModel::dislikeImageById($id, $userId);
        return false;
    }

    public static function createImage($userId, $imgInfo) {
        $decoded = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imgInfo->{'img'}->{'src'}));
        $plainImg = imagecreatefromstring($decoded);
        $filtername = $imgInfo->{'filter'} ? $imgInfo->{'filter'}->{'name'} : 0;
        $filter = 0;
        if ($filtername) {
            $filter = imagecreatefrompng('assets/' . $imgInfo->{'filter'}->{'name'});
            imagecopymerge($plainImg, $filter,
                $imgInfo->{'img'}->{'startPosX'},
                $imgInfo->{'img'}->{'startPosY'},
                $imgInfo->{'filter'}->{'startPosX'},
                $imgInfo->{'filter'}->{'startPosY'},
                $imgInfo->{'filter'}->{'width'},
                $imgInfo->{'filter'}-> {'height'},
                0
            );
        }
        $imgName = uniqid() . '.png';
        imagepng($plainImg, 'assets/' . $imgName);
        GalleryModel::saveImgInDatabase($userId, $imgName);
    }

    public static function getImageCount() {
        $connection = DB::getConnection();
        $sql = "SELECT COUNT(*) FROM  image";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute();
            return ($prepared->fetch(PDO::FETCH_BOTH)[0]);
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    public static function getImages($page, $countOnPage){
        $page = $page ? (int)$page : 1;
        $countOnPage = $countOnPage ? (int)$countOnPage : 6;
        $count = GalleryModel::getImageCount();
        $totalPage = ceil($count / $countOnPage);
        $pageCount = (($count - 1) / $countOnPage) + 1;
        //$page = $page > $pageCount ? $totalPage - 1 : $page;
        $start = $page * $countOnPage - $countOnPage;

        $connection = DB::getConnection();
        $sql = "SELECT * FROM image ORDER BY id DESC LIMIT ?, ?";
        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$start, $countOnPage]);

            $res = $prepared->fetchAll();
            return $res;
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

}
