<?php

class UserModel
{
    static private $data = array(
        'username' => array('value' => '', 'error' => ''),
        'email' => array('value' => '', 'error' => ''),
        'password' => array('value' => '', 'error' => ''),
        'confPassword' => array('value' => '', 'error' => ''),
        'notification' => array('value'=> true));

    public static function isValid($email, $username, $password, $confPassword) {
        $errors = 0;
        if (!$email || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            self::$data['email']['value'] = $email;
            self::$data['email']['error'] = 'wrong email  format';
            $errors++;
        }
        if (!$username) {
            self::$data['username']['value'] = $username;
            self::$data['username']['error'] = 'username require';
            $errors++;
        }
        if (!$password) {
            self::$data['password']['error'] = 'password require';
            $errors++;
        }
        if (!$confPassword){
            self::$data['confPassword']['error'] = 'Passwords are not same';
            $errors++;
        }
        if ($password !== $confPassword) {
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            self::$data['confPassword']['error'] = 'Passwords are not same';
            $errors++;
        }
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);

        if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            self::$data['confPassword']['error'] = 'Need one Upper Lower and 8 more';
            $errors++;
        }
        return $errors == 0;
    }

    public static function create($email, $username, $password, $confPassword){
        if (!self::isValid($email, $username, $password, $confPassword))
            return self::$data;
        $connection = DB::getConnection();
        $sql = "INSERT INTO `user` (email, username, password, activationLink)
                  VALUES (?, ?, ?, ?)";

        try {
            $password = md5($password);
            $activationLink = md5($password . 'makaka' . $password .time());
            $prepared = $connection->prepare($sql);
            $prepared->execute([trim($email), trim($username), $password, $activationLink]);
            mail($email, "Camagru Welcome",
                "Activate your account by this http://" . $_SERVER['HTTP_HOST'] . "/?activateLink=". $activationLink);
            return null;
        } catch(PDOException $e) {
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            $message = explode(' ',$e->getMessage());
            $message = str_replace("'", '', $message[count($message) - 1]);
            self::$data[$message]['error'] = "Already exist";
            return self::$data;
        }
    }

    public function activate($activateLink) {
        $connection = DB::getConnection();
        $sql = "UPDATE user SET isActive=TRUE , activationLink=NULL
                  WHERE activationLink=? AND isActive=0";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$activateLink]);
            return true;
        } catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function login($username, $password){
        if (!$username || !$password) {
            self::$data['username']['value'] = $username;
            self::$data['username']['error'] = 'Wrong username or Password';
            return self::$data;
        }

        $connection = DB::getConnection();
        $sql = "SELECT * FROM `user` WHERE username=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$username]);
            $user = $prepared->fetch();
            if (!$user || !$user['isActive']) {
                self::$data['username']['error'] = 'Wrong Username or Password';
                return self::$data;
            }
            if ($user['password'] !== md5($password)){
                self::$data['username']['error'] = 'Wrong Username or Password';
                return self::$data;
            }
        } catch(PDOException $e){
            echo $e->getMessage();
            self::$data['username']['error'] = 'some server Error';
            return self::$data;
        }

        return array('id' => $user['id'],
            'username' => $user['username'],
            'email' => $user['email'],
            'isNotification' => $user['isNotification']);
    }

    public function getUserById($id) {
        $connection = DB::getConnection();
        $sql = "SELECT * FROM user WHERE id=:id";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->bindParam(':id', $id, PDO::PARAM_INT);
            $prepared->execute();
            $res =  $prepared->fetch();
            return $res;
        } catch(PDOException $e) {
            return null;
        }
    }

    public function getUserByEmail($email) {
        $connection = DB::getConnection();
        $sql = "SELECT * FROM user WHERE email=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$email]);
            $res =  $prepared->fetch();
            return $res;
        } catch(PDOException $e) {
            return null;
        }
    }

    public static function resetLinkExist($resetLink) {
        if (!$resetLink)
            return null;
        $connection = DB::getConnection();
        $sql = "SELECT * FROM user WHERE resetLink=?";

        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([$resetLink]);
            $res = $prepared->fetch();
            return $res ? $res['resetLink'] : null;
        } catch(PDOException $e) {
            return null;
        }
    }

    public function resetPassword($link, $password, $password_conf) {
        if (!$password || $password !== $password_conf || !UserModel::resetLinkExist($link)) {
            self::$data['password']['error'] = 'Passwords are not same or wrong link';
            return self::$data;
        }
        $uppercase = $password ? preg_match('@[A-Z]@', $password) : true;
        $lowercase = $password ? preg_match('@[a-z]@', $password) : true;
        $number    = $password ? preg_match('@[0-9]@', $password) : true;
        $len = $password ? strlen($password) : 100;

        if (!$uppercase || !$lowercase || !$number || $len < 8) {
            self::$data['confPassword']['error'] = 'Need one Upper Lower and 8 more';
            return self::$data;
        }

        $connection = DB::getConnection();
        $sql = "UPDATE user SET password=?, resetLink=NULL WHERE resetLink=?";
        try {
            $prepared = $connection->prepare($sql);
            $prepared->execute([md5($password), $link]);
            $res = $prepared->fetch();
            return null;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function setResetLink($userId, $email) {
        $connection = DB::getConnection();
        $sql = "UPDATE user SET resetLink=? WHERE id=?";
        try {
            $link = md5(time() . $userId . 'super secret') . md5('secret' . time());
            $stmt= $connection->prepare($sql);
            $stmt->execute([$link, $userId]);
            mail($email,"Camagru Reset Password",
                "Reset password with this link http://" . $_SERVER['HTTP_HOST'] . "/?resetLink=". $link);
        } catch(PDOException $e) {

        }
    }

    public function logout()
    {
    }

    public static function sendResetLink($email) {
        $res = UserModel::getUserByEmail($email);
        if ($res) {
            UserModel::setResetLink($res['id'], $res['email']);
        }
    }

    public function get_editable_data($userId) {
        $data = UserModel::getUserById($userId);
        self::$data['username']['value'] = $data['username'];
        self::$data['email']['value'] = $data['email'];
        self::$data['notification']['value'] = $data['isNotification'] === 1 ? 1 : 0;
        return self::$data;
    }

    public static function editUserById($id, $email, $username, $password, $confPassword, $notification){
        $connection = DB::getConnection();
        if ($password !== $confPassword) {
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            self::$data['confPassword']['error'] = 'Passwords are not same';
        }

        $uppercase = $password ? preg_match('@[A-Z]@', $password) : true;
        $lowercase = $password ? preg_match('@[a-z]@', $password) : true;
        $number    = $password ? preg_match('@[0-9]@', $password) : true;
        $len = $password ? strlen($password) : 100;

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !$uppercase || !$lowercase || !$number || $len < 8) {
            self::$data['email']['error'] = !filter_var($email, FILTER_VALIDATE_EMAIL) ? "Wrong email format" : '';
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            self::$data['confPassword']['error'] = 'Need one Upper Lower and 8 more';
            return self::$data;
        }

        $password = $password ? md5($password): null;
        $notification = $notification ? 1 : 0;

        $sql = "UPDATE user SET email=COALESCE(?, email), username=COALESCE(?, username), password=COALESCE(?, password), isNotification=COALESCE(?, isNotification) WHERE id=?";
        try {
            $stmt= $connection->prepare($sql);
            $stmt->execute([trim($email), trim($username), $password, $notification, $id]);
            mail($email,"Camagru Account",
                "Account data was changed successfully.");
            return null;
        } catch(PDOException $e) {
            self::$data['username']['value'] = $username;
            self::$data['email']['value'] = $email;
            $message = explode(' ',$e->getMessage());
            $message = str_replace("'", '', $message[count($message) - 1]);
            self::$data[$message]['error'] = "Already exist";
            return self::$data;
        }
    }

}
