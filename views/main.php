<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Camagru</title>
        <link rel="stylesheet" type="text/css" href="/assets/style.css" />
    </head>
    <body>
    <div class="background-image"></div>
        <div class="header">
            <a href="/" class="logo">Camagru</a>
            <div class="header-right" id="header-buttons">
                <button class="button bordered" onclick="showPhotoMakerModal()" style="display: none">Photo</button>
                <button class="button bordered" onclick="showProfileEditModal()" style="display: none">Edit</button>
                <button class="button bordered" onclick="logout()"style="display: none">LogOut</button>
                <button class="button bordered" onclick="showLoginModal()" style="display: none">Login</button>
                <button class="button bordered" onclick="showSignUpModal()" style="display: none">SignUp</button>
            </div>
        </div>
        <div class="gallery" id="empty-gallery">
            No image here
        </div>
        <div class="gallery" id="gallery">
            <div class="left-arrow" onclick="getImages(-1)">
                <div class="box lefter">◀</div>
            </div>
            <div class="content" id="gallery-content">
            </div>
            <div class="right-arrow" onclick="getImages(1)">
                <div class="box">▶</div>
            </div>
        </div>

    <div id="signup-modal" class="modal">
        <div class="modal-container bordered">
            <div class="form-head">
                Sign Up
            </div>
            <div id="signUp" action="/user/create" method="POST" class="form-fields" style="max-height: 25rem !important;">
                <div class="form-field">
                    <input name="username" type="text" placeholder="Username" class="bordered input">
                    <div class="error"></div>
                </div>
                <div class="form-field">
                    <input name="email" type="email" placeholder="Email" class="bordered input">
                    <div class="error"></div>
                </div>
                <div class="form-field">
                    <input name="password" type="password" placeholder="Password" pattern="[A-Za-z0-9]{8,}" title="Should be 8 or more symbols" class="bordered input">
                    <div class="error"></div>
                </div>
                <div class="form-field">
                    <input name="password_conf" type="password" placeholder="Confirm Password" class="bordered input">
                    <div class="error"></div>
                </div>
                <button class="button bordered submit" onclick="onSignUp()">Sign Up</button>
            </div>
        </div>
    </div>

    <div id="login-modal" class="modal">
        <div class="modal-container bordered">
            <div class="form-head">
                Login
            </div>
            <div id="login" class="form-fields">
                <div class="form-field">
                    <input name="username" type="text" placeholder="Username" class="bordered input">
                    <div class="error"></div>
                </div>
                <div class="form-field">
                    <input name="password" type="password" placeholder="Password" pattern="[A-Za-z0-9]{8,}" title="Should be 8 or more symbols" class="bordered input">
                    <div class="error"></div>
                </div>
                <p><a href="javascript:;" onclick="showForgotModal()">Forgot Password?</a></p>
                <button class="button bordered submit" onclick="login()">Login</button>
            </div>
        </div>
    </div>

    <div id="reset-password" class="modal"  <?php echo ($resetLink ? 'style="display: flex"' : '') ?> >
        <div class="modal-container bordered">
            <div class="form-head">
                Reset Password
            </div>
            <div class="form-fields">
                <div class="form-field">
                    <input id="pswd" name="password" type="password" placeholder="Password" pattern="[A-Za-z0-9]{8,}" title="Should be 8 or more symbols" class="bordered input">
                    <div id="pswd-err" class="error"></div>
                </div>
                <div class="form-field">
                    <input id="conf-pswd" name="password_conf" type="password" placeholder="Confirm Password" class="bordered input">
                    <div id="conf-pswd-err" class="error"></div>
                </div>
                <button class="button bordered submit" onclick="resetPassword()">Reset</button>
            </div>
        </div>
    </div>

    <div id="forgot-password" class="modal">
        <div class="modal-container bordered">
            <div class="form-head">
                Forgot Password
            </div>
            <div class="form-fields">
                <div class="form-field">
                    <input name="email" type="email" placeholder="Email" class="bordered input" id="email-reset-value">
                    <div class="error"></div>
                </div>
                <button class="button bordered submit" onclick="sendEmailReset()">Send Email</button>
            </div>
        </div>
    </div>

    <div id="profile-edit" class="modal">
        <div class="modal-container bordered">
            <div class="form-head">
                Edit Profile
            </div>
            <div id="editValSec" class="form-fields">
                <div class="form-field">
                    <input name="username" type="text" placeholder="Username" class="bordered input">
                    <div class="error"></div>
                </div>
                <div class="form-field">
                    <input name="email" type="email" placeholder="Email" class="bordered input">
                    <div class="error"> </div>
                </div>
                <div class="form-field">
                    <input name="password" type="password" placeholder="Password" pattern="[A-Za-z0-9]{8,}" title="Should be 8 or more symbols" class="bordered input">
                    <div class="error"> </div>
                </div>
                <div class="form-field">
                    <input name="password_conf" type="password" placeholder="Confirm Password" class="bordered input">
                    <div class="error"> </div>
                </div>
                <div class="form-field">
                    <input name="notification" type="checkbox" class="checkbox" > Notifications
                </div>
                <button class="button bordered submit" style="margin-top: 0.9rem!important;" onclick="editUserInfo()">Edit</button>
            </div>
        </div>
    </div>

    <div id="photo-viewer" class="modal">
        <ul class="modal-container bordered" style="padding:1rem !important; max-height: 25rem; overflow-y: scroll;">
            <li class="img-view-container">
                <div class="img-view-body">
                    <div class="section">
                        <div class="image">
                            <img src="https://images-eds-ssl.xboxlive.com/image?url=8Oaj9Ryq1G1_p3lLnXlsaZgGzAie6Mnu24_PawYuDYIoH77pJ.X5Z.MqQPibUVTc6GgVE.wL.kjn_J5aFD3XibWAIWMsb5x_ldfxxzGWkMmWrr08x5gmtUCucOTZY27B9THtOcAxbpg0gxysUNuC9OQ_dfa0V6jptdgVdnMk.og5IqCroPMrwBf9D3QD.DXOQlHDbppuAMLNbR_xvLEr5w.k0JcBqd9IHk3NEkf8cTc-&h=1080&w=1920&format=jpg" alt="Img" id="view-img">
                        </div>
                        <div class="img-view-like" id="like" onclick="likeIt()">
                            ❤
                            <span id="like-count">0 Likes</span>
                        </div>
                    </div>
                    <div class="section" style="padding-top: 0.5rem">
                        <ul class="comments" id="comments">
                        </ul>
                        <textarea class="img-view-inputText" placeholder="Leave your comment here" id="new-comment"></textarea>
                        <button class="button bordered" style="margin-top: 1rem; margin-bottom: 1rem;" onclick="sendComment()">Send</button>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div id="photo-maker" class="modal">
        <ul class="modal-container bordered" style="padding:1rem !important; max-height: 30rem; overflow-y: scroll;">
            <li class="img-view-container">
                <canvas id="video-player" onclick="makePhoto()"></canvas>
                <div class="img-list" style="margin-top: 0.4rem">
                    <img src="/assets/b.png" alt="b.png" onclick="selectFilter(event)" data-type="border"/>
                    <img src="/assets/e.png" alt="e.png" onclick="selectFilter(event)" data-type="filt"/>
                    <img src="/assets/h.png" alt="h.png" onclick="selectFilter(event)" data-type="filt"/>
                </div>
                <div class="img-list" style="margin-top: 0.4rem" id="photo-set">
                </div>
                <div class="button-list" style="margin-top: 0.4rem">
                    <button class="button" id="upload-image" onclick="uploadImage()">Upload</button>
                    <button class="button" id="load-image" onclick="loadFile()">Load</button>
                </div>
            </li>
        </ul>
    </div>

    <div class="footer" id="footer">
        <span> &copy; 2019 - Camagru Made by <a href="https://profile.intra.42.fr/users/osak">osak</a></span>
    </div>
    </body>
    <script type="text/javascript" src="/assets/index.js"></script>
</html>