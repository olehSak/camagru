<?php
include_once 'IRequest.php';

class Request implements IRequest
{
    private $params;
    private $body;

    function __construct()
    {
        $this->formatData();
    }

    private function formatData()
    {
        $this->params = $_GET;
        if (strpos($_SERVER["CONTENT_TYPE"], 'application/json') !== false) {
            $this->body = $this->parseJson();
        }
        else {
            $this->body = $_POST;
        }
        $this->formatRequestInfo();
        foreach($this->body as $key => $value)
        {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }

    private function formatRequestInfo(){
        foreach($_SERVER as $key => $value)
        {
            $this->{$this->toCamelCase($key)} = $value;
        }
        $this->requestUri = $this->pathInfo;
    }

    private function toCamelCase($string)
    {
        $result = strtolower($string);

        preg_match_all('/_[a-z]/', $result, $matches);
        foreach($matches[0] as $match)
        {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }
        return $result;
    }

    private function parseJson() {
        return json_decode(file_get_contents("php://input"));
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getParams()
    {
        return $this->params;
    }
}
