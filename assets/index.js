console.log = () => {};
const GLOBAL_STATE = {
    currentUser: null,
    isImageHere: false,
    makedPhotoInfo: [],
	currentObject: {
		body: null,
		close: () => {},
	},
	selectedImageIndex: 0,
    videoSettings: {
        audio: false,
        video: {
            // width: {min: 1280},
            // height: {min: 720}
        }
    },
	forms: {
		signUp: {
			username: {
				value: '',
				error: ''
			},
            email: {
                value: '',
                error: ''
            },
			password: {
				value: '',
				error: ''
			},
			confPassword:{
                value: '',
                error: ''
			}
		},
		login: {
            username: {
                value: '',
                error: ''
            },
            password: {
                value: '',
                error: ''
            },
		},
        edit: {
            username: {
                value: '',
                error: ''
            },
            email: {
                value: '',
                error: ''
            },
            password: {
                value: '',
                error: ''
            },
            confPassword:{
                value: '',
                error: ''
            },
            notification: {
                value: true,
            }
        },
	},
	objects: {
		headerButtons: null,
		signupModal: null,
		loginModal: null,
		forgotPassword: null,
		frofileEdit: null,
		photoViewer: null,
		gallery: null,
		galleryContent: null,
		emptyGallery: null,
		like: null,
		comments: null,
		likeCount: null,
		viewImage: null,
		newComment: null,
        photoSet: null,
        videoPlayer: null,
        video: null,
        context: null,
        currentCatchObject: null,
        currentFilter: null,
        videoStream: null
	},
	gallery: {
		page: 0,
		count: 6,
		images: []
	}
};

class RequestService {
	static async getImageInfo(id) {
        let res = await fetch(`/gallery/image?id=${id}`, {
                headers: {
                    'Accept': 'application/json',
                },
                method: "GET",
            });
            return res.json();
	}

    static async forgotPassword(email) {
        return fetch(`/forgotPassword?email=${email}`, {
                headers: {
                    'Accept': 'application/json',
                },
                method: "POST",
            });
    }

    static async resetPassword(resetLink, password, confPassword) {
        console.log({resetLink, password, confPassword});
        let res = await fetch(`/resetPassword?resetLink=${resetLink}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({password, confPassword}),
            });
        return res.json();
    }

	static async sendComment(comment, imgId) {
		let res = await fetch(`/gallery/image/comment?id=${imgId}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({text: comment}),
            });
	}

	static async likeImage(imgId) {
        console.log(imgId);
        let res = await fetch(`/gallery/image/like?id=${imgId}`, {
                headers: {
                    'Accept': 'application/json',
                },
                method: "POST",
            });
        return (await res.json()).like;
	}

	static async uploadImage(url, info) {
        info.img.src = url;
        const res = await fetch("/gallery", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(info)
        });
    }

    static async signUp(username, email, password, confPassword) {
        try{
            let res = await fetch(`/signup`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ username, email, password, confPassword}),
            });
            return await res.json();
        } catch(e) {
            console.log('errrrr', e);
        }
	}

	static async login(username, password) {

        try{
            let res = await fetch(`/login`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ username, password}),
            });
            let data = await res.json();
            console.log(data);
            if (!data.id)
                return data;
            else {
                GLOBAL_STATE.currentUser = data;
                localStorage.setItem('currentUser', JSON.stringify(GLOBAL_STATE.currentUser));
                return null;
            }
        } catch(e) {
            console.log('errrrr', e);
        }
	}

    static async logout() {
	    await fetch(`/logout`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET",
        });
    }

    static async edit(username, email, password, confPassword, isNotification) {
        console.log({username, email, password, confPassword, isNotification});

        let res = await fetch(`/edit`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ username, email, password, confPassword, isNotification}),
            });
        return res.json();
    }

    static async getMyInfo(){
        return {
            username: {
                value: 'lol',
                error: 'this is error11'
            },
            email : {
                value: 'lol@email.com',
                error: 'this is error2'
            },
            password: {
                value: '12341241',
                error: 'this is error'
            },
            confPassword: {
                value: '12321414',
                error: 'this is error'
            },
            notification: {
                value: true,
            }
        }
    }

    static async getImages(page, count) {
        let res = await fetch(`/gallery?page=${page}&count=${count}`, {
            headers: {
                'Accept': 'application/json'
            },
            method: "GET"
        });
        return res.json();
    }

    static async removeImage(id) {
        let res = await fetch(`/gallery/image?id=${id}`, {
            headers: {
                'Accept': 'application/json'
            },
            method: "DELETE"
        });
        return res.json();
    }
}


window.onclick = function(event) {
    if (event.target === GLOBAL_STATE.currentObject.body) {
    	GLOBAL_STATE.currentObject.close();
    }
};

const initButtons = () => {
    const buttons = GLOBAL_STATE.objects.headerButtons;

    GLOBAL_STATE.objects.buttonPhoto = buttons.children[0];
    GLOBAL_STATE.objects.buttonEdit = buttons.children[1];
    GLOBAL_STATE.objects.buttonLogout = buttons.children[2];
    GLOBAL_STATE.objects.buttonLogin = buttons.children[3];
    GLOBAL_STATE.objects.buttonSignUp = buttons.children[4];
    setHeaderButtons();
};

const initForms = () => {
	const signUp = GLOBAL_STATE.objects.signUp;
    const signUpVal = GLOBAL_STATE.forms.signUp;
    const login = GLOBAL_STATE.objects.login;
    const loginVal = GLOBAL_STATE.forms.login;
    const editValSec = GLOBAL_STATE.objects.editValSec;
    const editVal = GLOBAL_STATE.forms.edit;

    signUpVal.username.value = signUp.children[0].children[0];
    signUpVal.username.error = signUp.children[0].children[1];
    signUpVal.email.value = signUp.children[1].children[0];
    signUpVal.email.error = signUp.children[1].children[1];
    signUpVal.password.value = signUp.children[2].children[0];
    signUpVal.password.error = signUp.children[2].children[1];
    signUpVal.confPassword.value = signUp.children[3].children[0];
    signUpVal.confPassword.error = signUp.children[3].children[1];

    loginVal.username.value = login.children[0].children[0];
    loginVal.username.error = login.children[0].children[1];
    loginVal.password.value = login.children[1].children[0];
    loginVal.password.error = login.children[1].children[1];

    editVal.username.value = editValSec.children[0].children[0];
    editVal.username.error = editValSec.children[0].children[1];
    editVal.email.value = editValSec.children[1].children[0];
    editVal.email.error = editValSec.children[1].children[1];
    editVal.password.value = editValSec.children[2].children[0];
    editVal.password.error = editValSec.children[2].children[1];
    editVal.confPassword.value = editValSec.children[3].children[0];
    editVal.confPassword.error = editValSec.children[3].children[1];
    editVal.notification.value = editValSec.children[4].children[0];
    console.log('valuee', editVal.notification.value.value);
};

const initScene = () => {
	GLOBAL_STATE.objects.headerButtons = document.getElementById("header-buttons");
	GLOBAL_STATE.objects.signupModal = document.getElementById("signup-modal");
    GLOBAL_STATE.objects.resetPasswordModal = document.getElementById("reset-password");
	GLOBAL_STATE.objects.loginModal = document.getElementById("login-modal");
	GLOBAL_STATE.objects.forgotPassword = document.getElementById("forgot-password");
	GLOBAL_STATE.objects.frofileEdit = document.getElementById("profile-edit");
    GLOBAL_STATE.objects.editValSec = document.getElementById('editValSec');
	GLOBAL_STATE.objects.photoMaker = document.getElementById('photo-maker');
	GLOBAL_STATE.objects.photoViewer = document.getElementById("photo-viewer");
	GLOBAL_STATE.objects.gallery = document.getElementById("gallery");
	GLOBAL_STATE.objects.galleryContent = document.getElementById("gallery-content");
	GLOBAL_STATE.objects.emptyGallery = document.getElementById("empty-gallery");
	GLOBAL_STATE.objects.like = document.getElementById("like");
    GLOBAL_STATE.objects.signUp = document.getElementById('signUp');
    GLOBAL_STATE.objects.login = document.getElementById('login');
	GLOBAL_STATE.objects.comments = document.getElementById("comments");
	GLOBAL_STATE.objects.likeCount = document.getElementById("like-count");
	GLOBAL_STATE.objects.viewImage = document.getElementById("view-img");
	GLOBAL_STATE.objects.newComment = document.getElementById("new-comment");
    GLOBAL_STATE.objects.photoSet = document.getElementById("photo-set");
    GLOBAL_STATE.objects.videoPlayer = document.getElementById("video-player");
    GLOBAL_STATE.objects.video = document.createElement('video');
    GLOBAL_STATE.objects.context = GLOBAL_STATE.objects.videoPlayer.getContext('2d');
    GLOBAL_STATE.objects.fileLoad = document.createElement('input');

    GLOBAL_STATE.objects.fileLoad.setAttribute('accept',"image/jpeg, image/png, image/jpg");
    GLOBAL_STATE.objects.fileLoad.addEventListener("change", getFile, false);
    GLOBAL_STATE.objects.fileLoad.type = 'file';
    GLOBAL_STATE.objects.video.setAttribute('autoplay', true);
    GLOBAL_STATE.currentUser = JSON.parse(localStorage.getItem('currentUser'));
};

const getNodes = str => new DOMParser().parseFromString(str, 'text/html').body.childNodes;

const generateNewGalleryImage = (img, onclick) => {
	const node = getNodes(`
		<div class="image" onclick="${onclick}" style="position: relative;">
            <img src="${'/assets/' + img.name}" alt="Img">
            ${GLOBAL_STATE.currentUser && img.user === GLOBAL_STATE.currentUser.id ? '<div class="rm-img" style="position: absolute; border 0.01rem solid red; color: red; top: 0.1rem; left: 0.1rem; ">X</div>' : ''}
        </div>
	`)[0];
	node.addEventListener("click", onclick);
    if (GLOBAL_STATE.currentUser && img.user === GLOBAL_STATE.currentUser.id)
        node.children[1].addEventListener("click", (event) => { event.stopPropagation(); removeImageFromGallery(img.id, node)});
	return node;
};

const generateComment = (comment) => {
	const node = getNodes(`
		<li class="comment">
            <div class="author"></div>
            <span></span>
       	</li>
	`)[0];
	node.children[0].innerText = comment.username;
	node.children[1].innerText = comment.text;
	return node;
};

const generateMakedImage = (src, filterName, imgIndex) => {
    const node = getNodes(`
		<div id="container" style="position: relative;">
    		<img src="${src}" crossorigin='anonymous' style="width: 7rem; height: 7rem;" alt="${filterName}"/>
    		<div class="rm-img" style="position: absolute; border 0.01rem solid red; color: red; top: 0.1rem; left: 0.1rem; ">X</div>
		</div>
	`)[0];
    node.children[1].addEventListener("click", () => {node.remove(); GLOBAL_STATE.makedPhotoInfo.splice(imgIndex, 1);});
    return node;
};


//MAYBE ADD REDROW AND CHEK IF NO IMAGES TO LAZY WORK JUS SET EMPTY GALLERY COMPONENT
const redrawGallery = () => {
	if (GLOBAL_STATE.gallery.images.length > 0) {
		GLOBAL_STATE.objects.gallery.style.display = "flex";
		GLOBAL_STATE.objects.emptyGallery.style.display = "none";

        console.log(GLOBAL_STATE.objects.galleryContent.innerHtml);
		GLOBAL_STATE.objects.galleryContent.innerHTML = "";
		GLOBAL_STATE.gallery.images.forEach((img, index) => {
            console.log(img, index);
			GLOBAL_STATE.objects.galleryContent.appendChild(generateNewGalleryImage(img, () => showImageViewModal(index)))
		})
	}
};

const tryToGetCameraVideo = async () => {
    try {
        const stream = await navigator.mediaDevices.getUserMedia(GLOBAL_STATE.videoSettings);
        GLOBAL_STATE.objects.video.srcObject = stream;
        GLOBAL_STATE.objects.video.addEventListener('play', videoPlayEvent);
        GLOBAL_STATE.objects.currentCatchObject = GLOBAL_STATE.objects.video;
        GLOBAL_STATE.isImageHere = true;
    } catch(e) {
        console.log(e);
    }
    renderCameraVideo();
};

const renderCameraVideo = () => {
    const videoPlayer = GLOBAL_STATE.objects.videoPlayer;
    const context = GLOBAL_STATE.objects.context;
    const filter = GLOBAL_STATE.objects.currentFilter;
    const video =  GLOBAL_STATE.objects.currentCatchObject;

    if (video) {
        context.drawImage(video, 0, 0, videoPlayer.width, videoPlayer.height);
        if (filter){
            const startPosX = filter.target.dataset.type === "filt" ? videoPlayer.width / 2 - 100 : 0;
            const startPosY = filter.target.dataset.type === "filt" ? videoPlayer.height / 2 - 130 : 0;
            const width = filter.target.dataset.type === "filt" ? 200 : videoPlayer.width;
            const height = filter.target.dataset.type === "filt" ? 200 : videoPlayer.height;
            // console.log({startPosX, startPosY, width, height, lol: filter.target.dataset.type});
            context.drawImage(filter.target, startPosX, startPosY, width, height);
        }
    }

    setTimeout(renderCameraVideo,5);
};

const videoPlayEvent = () => {
    console.log('event');
    renderCameraVideo();
};

const readUploadedFileAsText = (inputFile) => {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result);
        };
        temporaryFileReader.readAsDataURL(inputFile);
    });
};


const getFile = async (event) => {
    let data = await readUploadedFileAsText(event.target.files[0]);
    GLOBAL_STATE.objects.currentCatchObject = document.createElement('img');
    GLOBAL_STATE.objects.currentCatchObject.setAttribute('crossorigin', 'anonymous');
    GLOBAL_STATE.objects.currentCatchObject.src = data;
    GLOBAL_STATE.isImageHere = true;
};

const loadFile = () => {
    GLOBAL_STATE.objects.fileLoad.click();
};

const uploadFile = () => {

};

const getImages = async (offset) => {

	const gallery = GLOBAL_STATE.gallery;

    const pageNumber = gallery.page + offset;
    if (pageNumber < 1)
        return;
    console.log(pageNumber);
    try {
        const img = await RequestService.getImages(pageNumber, gallery.count);
        console.log(img);
        if (img.length) {
            gallery.images = img;
            redrawGallery();
            gallery.page += offset;
        }
    } catch (e) {
        console.log(e);
    }
};

function closeSignUpModal() {
	GLOBAL_STATE.objects.signupModal.style.display = "none";
}

function showSignUpModal() {
	GLOBAL_STATE.currentObject.close();
	GLOBAL_STATE.objects.signupModal.style.display = "flex";
	GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.signupModal;
	GLOBAL_STATE.currentObject.close = closeSignUpModal;
}

function closeLoginModal() {
	GLOBAL_STATE.objects.loginModal.style.display = "none";
}

function showLoginModal() {
	GLOBAL_STATE.currentObject.close();
	GLOBAL_STATE.objects.loginModal.style.display = "flex";
	GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.loginModal;
	GLOBAL_STATE.currentObject.close = closeLoginModal;
}

function closeForgotModal() {
	GLOBAL_STATE.objects.forgotPassword.style.display = "none";
}

function showForgotModal() {
	GLOBAL_STATE.currentObject.close();
	GLOBAL_STATE.objects.forgotPassword.style.display = "flex";
	GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.forgotPassword;
	GLOBAL_STATE.currentObject.close = closeForgotModal;
}

function closeProfileEditModal() {
	GLOBAL_STATE.objects.frofileEdit.style.display = "none";
}

async function showProfileEditModal() {
    if (GLOBAL_STATE.currentUser) {
    	GLOBAL_STATE.currentObject.close();
    	GLOBAL_STATE.objects.frofileEdit.style.display = "flex";
    	GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.frofileEdit;
    	GLOBAL_STATE.currentObject.close = closeProfileEditModal;

        const edit = GLOBAL_STATE.forms.edit;
        edit.username.value.value = GLOBAL_STATE.currentUser.username;
        edit.email.value.value = GLOBAL_STATE.currentUser.email;
        edit.notification.value.checked = GLOBAL_STATE.currentUser.isNotification;
    }
}

function closePhotoMakerModal() {
    GLOBAL_STATE.objects.photoMaker.style.display = "none";
}

function showPhotoMakerModal() {
    if (GLOBAL_STATE.currentUser) {
        tryToGetCameraVideo();
        GLOBAL_STATE.currentObject.close();
        GLOBAL_STATE.objects.photoMaker.style.display = "flex";
        GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.photoMaker;
        GLOBAL_STATE.currentObject.close = closePhotoMakerModal;
    }
}


function closeImageViewModal() {
	GLOBAL_STATE.objects.photoViewer.style.display = "none";
}

function setHeaderButtons(){
    const isUser = GLOBAL_STATE.currentUser !== null;

    console.log(isUser, GLOBAL_STATE.objects.buttonPhoto);

    GLOBAL_STATE.objects.buttonPhoto.style.display = isUser ? 'block' : 'none';
    GLOBAL_STATE.objects.buttonEdit.style.display  = isUser ? 'block' : 'none';
    GLOBAL_STATE.objects.buttonLogout.style.display  = isUser ? 'block' : 'none';
    GLOBAL_STATE.objects.buttonLogin.style.display  = isUser ? 'none' : 'block';
    GLOBAL_STATE.objects.buttonSignUp.style.display  = isUser ? 'none' : 'block';
}

async function showImageViewModal(index) {
    if (GLOBAL_STATE.currentUser) {
        GLOBAL_STATE.objects.comments.innerHTML = '';
    	GLOBAL_STATE.currentObject.close();
    	GLOBAL_STATE.objects.photoViewer.style.display = "flex";
    	GLOBAL_STATE.currentObject.body = GLOBAL_STATE.objects.photoViewer;
    	GLOBAL_STATE.currentObject.close = closeImageViewModal;

        GLOBAL_STATE.selectedImageIndex = index;
    	const clickedImage = GLOBAL_STATE.gallery.images[index];
    	GLOBAL_STATE.objects.viewImage.src = '/assets/' + clickedImage.name;
    	const imgInfo = await RequestService.getImageInfo(clickedImage.id);
        clickedImage.likes = imgInfo.likes;
    	GLOBAL_STATE.objects.likeCount.innerText = imgInfo.likes + " Likes";
    	if(imgInfo.myLike) {
    		like.classList.add("liked");
    	}
    	else {
    		like.classList.remove("liked");
    	}
        console.log(imgInfo.comments)
    	imgInfo.comments.forEach(imgComment => {
    		GLOBAL_STATE.objects.comments.appendChild(generateComment(imgComment))
    	})
    }  
}

async function sendComment() {
	try {
        const imgId = GLOBAL_STATE.gallery.images[GLOBAL_STATE.selectedImageIndex].id
        if (GLOBAL_STATE.objects.newComment.value) {
            const comment = await RequestService.sendComment(GLOBAL_STATE.objects.newComment.value, imgId);
            console.log(GLOBAL_STATE.currentUser);
            GLOBAL_STATE.objects.comments.appendChild(generateComment({
                username: GLOBAL_STATE.currentUser.username,
                text: GLOBAL_STATE.objects.newComment.value
            }));
            GLOBAL_STATE.objects.newComment.value = '';
        }
	} catch(e) {
	}
}

async function likeIt() {
	try {
        const likeImg = GLOBAL_STATE.gallery.images[GLOBAL_STATE.selectedImageIndex];
		const isLike = await RequestService.likeImage(likeImg.id);
        likeImg.likes = isLike ? likeImg.likes + 1 : likeImg.likes - 1;
        console.log(likeImg);
		GLOBAL_STATE.objects.likeCount.innerText = likeImg.likes + " Likes";
		if(isLike) {
			like.classList.add("liked");
		}
		else {
			like.classList.remove("liked");
		}
	} catch(e) {
		console.log(e);
	}
}

async function onSignUp(event) {
	const signUp = GLOBAL_STATE.forms.signUp;

	try {
        const lol = await RequestService.signUp(
        	signUp.username.value.value,
            signUp.email.value.value,
            signUp.password.value.value,
            signUp.confPassword.value.value
			);
        console.log(lol);
		if (lol) {
            signUp.username.value.value = lol.username.value;
            signUp.email.value.value = lol.email.value;
            signUp.username.error.innerHTML = lol.username.error;
            signUp.email.error.innerHTML = lol.email.error;
            signUp.password.error.innerHTML = lol.password.error;
            signUp.confPassword.error.innerHTML = lol.confPassword.error;
        }
        else
            GLOBAL_STATE.currentObject.close();
    } catch(e) {
		console.log(e, signUp);
	}
}

async function editUserInfo(){
    const edit = GLOBAL_STATE.forms.edit;

    edit.username.error.innerHTML = '';
    edit.email.error.innerHTML = '';
    edit.password.error.innerHTML = '';
    edit.confPassword.error.innerHTML = '';

    try {

        const lol = await RequestService.edit(
            edit.username.value.value,
            edit.email.value.value,
            edit.password.value.value,
            edit.confPassword.value.value,
            edit.notification.value.checked
            );
        if (lol) {
            edit.username.value.value = lol.username.value;
            edit.email.value.value = lol.email.value;
            edit.username.error.innerHTML = lol.username.error;
            edit.email.error.innerHTML = lol.email.error;
            edit.password.error.innerHTML = lol.password.error;
            edit.confPassword.error.innerHTML = lol.confPassword.error;
            edit.notification.value.checked = lol.notification.value;
        }else{
            GLOBAL_STATE.currentUser.username=edit.username.value.value.trim() || GLOBAL_STATE.currentUser.username;
            GLOBAL_STATE.currentUser.email=edit.email.value.value || GLOBAL_STATE.currentUser.email;
            GLOBAL_STATE.currentUser.isNotification=edit.notification.value.checked
            localStorage.setItem('currentUser', JSON.stringify(GLOBAL_STATE.currentUser));
        }
    } catch(e) {
        console.log(e, edit);
    }
}

async function login(event) {
    const login = GLOBAL_STATE.forms.login;

    try {
        const lol = await RequestService.login(
            login.username.value.value,
            login.password.value.value
        );
        login.username.value.value = '';
        login.password.value.value = '';
        login.username.error.innerHTML = '';
        if (lol) {
            login.username.value.value = lol.username.value;
            login.username.error.innerHTML = lol.username.error;
        }
        else{
            GLOBAL_STATE.currentObject.close();
            setHeaderButtons();
            redrawGallery();
        }
        console.log('signUP', lol);
    } catch(e) {
        console.log(e, signUp);
    }
}

async function sendEmailReset() {
    const input = document.getElementById('email-reset-value');
    
    try {
        const lol = await RequestService.forgotPassword(input.value);
        GLOBAL_STATE.currentObject.close();
    } catch(e) {}
}

async function resetPassword(){
    try {
        const pswd = document.getElementById('pswd').value;
        const confPswd = document.getElementById('conf-pswd').value;
        const pswdErr = document.getElementById('pswd-err');
        const confPswdErr = document.getElementById('conf-pswd-err');
        res = await RequestService.resetPassword((new URLSearchParams(window.location.search)).get('resetLink'), pswd, confPswd);
        console.log(res);
        if (!res)
            window.location = window.location.origin;
        else {
            pswdErr.innerHTML = res.password.error || res.confPassword.error;
        }
    } catch (e){}
}

async function logout () {
    try {
        await RequestService.logout();
    } catch(e) {
        console.log(e, signUp);
    }
    localStorage.removeItem('currentUser');
    GLOBAL_STATE.currentUser = null;
    setHeaderButtons();
    redrawGallery();
}

function selectFilter (event) {
    GLOBAL_STATE.objects.currentFilter = event;
}

async function uploadImage() {
    const container = GLOBAL_STATE.objects.photoSet.children[0] ? GLOBAL_STATE.objects.photoSet.children[0].children[0] : null;
    if (container) {
        try {
            console.log(GLOBAL_STATE.makedPhotoInfo);
            await RequestService.uploadImage(container.src, GLOBAL_STATE.makedPhotoInfo[0]);
            // GLOBAL_STATE.makedPhotoInfo.shift();
            GLOBAL_STATE.objects.photoSet.children[0].children[1].click();
            console.log(GLOBAL_STATE.objects.photoSet);
            console.log(GLOBAL_STATE.makedPhotoInfo);
        } catch(e) {
            console.log(e);
        }
    }
}

async function removeImageFromGallery(id, node) {
    try {
        const res = await RequestService.removeImage(id);
        node.remove();
        // redrawGallery();
    }catch(e) {console.log(e)}
}

function makePhoto() {
    if (!GLOBAL_STATE.isImageHere)
        return;
	const photoSet = GLOBAL_STATE.objects.photoSet;
	const player = GLOBAL_STATE.objects.videoPlayer;
    const filter = GLOBAL_STATE.objects.currentFilter;
    const videoPlayer = GLOBAL_STATE.objects.videoPlayer;
	const filterName = filter ? GLOBAL_STATE.objects.currentFilter.target.alt : null;

	console.log('data', player.toDataURL('image/png'));
    GLOBAL_STATE.makedPhotoInfo.push({
        img: {
           startPosX: 0,
           startPosY: 0,
           width: videoPlayer.width,
           height: videoPlayer.height
        },
        filter: filter ? {
            name: filterName,
            startPosX: filter.target.dataset.type === "filt" ? videoPlayer.width / 2 - 100 : 0,
            startPosY: filter.target.dataset.type === "filt" ? videoPlayer.height / 2 - 130 : 0,
            width: filter.target.dataset.type === "filt" ? 200 : videoPlayer.width,
            height: filter.target.dataset.type === "filt" ? 200 : videoPlayer.height
        } : {}
    });
	photoSet.appendChild(generateMakedImage(player.toDataURL('image/png'), filterName, GLOBAL_STATE.makedPhotoInfo.length - 1));
    GLOBAL_STATE.objects.currentCatchObject = GLOBAL_STATE.objects.video;
    GLOBAL_STATE.objects.currentFilter = null;
}

initScene();
initButtons();
initForms();
getImages(1);
